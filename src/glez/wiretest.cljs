(ns glez.wiretest
  (:require [glez.wires :as wires]
            [glez.model :as m]
            [glez.scene :as sc]
            [glez.loop :as l]
            [linjar.mat4 :as mat4]
            [linjar.mat3 :as mat3]
            [linjar.vec3 :as vec3]
            [linjar.vec2 :as vec2]
            [linjar.camera :as cam]
            [glez.utils :as u]))

(defn init
  [id width height]
    (let [[canvas gl] (u/init-canvas id width height)
          {program :program
           attributes :attributes
           {:keys [uWorldView uModelView] :as uniforms} :uniforms}
              (u/create-program gl wires/shader)

          projection (mat4/perspective 45 width height 0.1 100)
          modelView (mat4/from-3d (mat3/rotation-y (/ Math/PI 8)))
          camera (mat4/face-position [0 0 1] [0 0 0] [0 1 0])
          cube (m/load gl (m/gen-box [0 0 0] 0.5 0.5 0.5))
          sphere (m/load gl (m/gen-sphere 1 36 18))]

      (. gl useProgram program)
      (. gl uniformMatrix4fv uWorldView
                             false
                             (js/Float32Array. (mat4/multiply projection
                                                              camera)))
      (. gl uniformMatrix4fv uModelView
                             false
                             (js/Float32Array. modelView))
      (. gl clearColor 0.25 0.5 0.55 1.0)
      (. gl clear (bit-or (.-COLOR_BUFFER_BIT gl) (.-DEPTH_BUFFER_BIT gl)))
      (wires/draw gl attributes uniforms cube)))

(init "output" 640 360)
