(ns glez.shader)

(def symbol-type cljs.core/Symbol)
(def list-type cljs.core/List)
(def keyword-type cljs.core/Keyword)
(def operators #{'* '+ '/ ' -})

(declare parse-statement)
(defmulti parse-expression type)
(defmethod parse-expression list-type
  [expr]
  (let [[op & operands] expr]
    (if (contains? operators op)
      (do (print "(")
          (parse-expression (first operands))
          (doseq [operand (rest operands)]
            (print op)
            (parse-expression operand))
          (print ")"))
      (do (print (str op "("))
          (when (first operands)
            (parse-expression (first operands))
            (doseq [operand (rest operands)]
              (print ",")
              (parse-expression operand)
              ))
          (print ")")))))

(defmethod parse-expression keyword-type
  [expr]
  (print (name expr)))
(defmethod parse-expression symbol-type
  [sym]
  (let [s (str sym)]
    (if (clojure.string/starts-with? s "f.")
      (print (str (clojure.string/join (drop 2 s)) ".0"))
      (print s))))
(defmethod parse-expression :default
  [expr]
  (print expr))

(defmulti parse-assignee type)
(defmethod parse-assignee list-type
  [assignee]
  (print (clojure.string/join " " assignee)))

(defmethod parse-assignee keyword-type
  [assignee]
  (print (name assignee)))

(defmethod parse-assignee :default
  [assignee]
  (print assignee))
(defn parse-assignment
  [[assignee expression]]
  (do (parse-assignee assignee)
      (print " = ")
      (parse-expression expression)
      (println ";")))

(defn parse-function
  [[type name args & body]]
  (do (print type name)
      (print "(")
      (when (not (empty? args))
        (print (first args))
        (doseq [arg (rest args)]
          (print "," arg)))
      (println "){")
      (doseq [expr body]
        (print "  ")
        (parse-statement expr))
      (println "}")))

(defn parse-declaration
  [declaration]
  (do
    (print (first declaration))
    (doseq [word (rest declaration)]
      (print " ")
      (if (= keyword-type (type word))
        (print (name word))
        (print word)))
      (println ";")))

(defn parse-statement
  [[head & tail :as statement]]
  (case head
    assign (parse-assignment tail)
    function (parse-function tail)
    (parse-declaration statement)))

(defn clj->shader
  [prog]
  (with-out-str
    (doseq [expr prog] (parse-statement expr))))



