(ns glez.shapes)

(defn draw-shape
  [gl {:keys [aPosition]} {:keys [positions indices n]}]
  (. gl bindBuffer (.-ARRAY_BUFFER gl) positions)
  (. gl vertexAttribPointer aPosition
                            3
                            (.-FLOAT gl)
                            false
                            0
                            0)
  (. gl enableVertexAttribArray aPosition)
  (. gl bindBuffer (.-ELEMENT_ARRAY_BUFFER gl) indices)

  (. gl drawElements (.-TRIANGLES gl) n (.-UNSIGNED_SHORT gl) 0))

(defn init-shape
  [gl {:keys [points indices n]}]
  (let [position-buffer (. gl createBuffer)
        positions (js/Float32Array. (flatten points))
        index-buffer (. gl createBuffer)
        index-data (js/Uint16Array. (flatten indices))]
    (. js/console log "positions:" positions)
    (. js/console log "indices:"  index-data)
    (. gl bindBuffer (.-ARRAY_BUFFER gl) position-buffer)
    (. gl bufferData (.-ARRAY_BUFFER gl)
                      positions
                     (.-STATIC_DRAW gl))

    (. gl bindBuffer (.-ELEMENT_ARRAY_BUFFER gl) index-buffer)
    (. gl bufferData (.-ELEMENT_ARRAY_BUFFER gl)
                      index-data
                     (.-STATIC_DRAW gl))
    {:n n
     :positions position-buffer
     :indices index-buffer}))

(def cube {:points [[-1.0 -1.0 1.0]
                    [1.0 -1.0 1.0]
                    [1.0 1.0 1.0]
                    [-1.0 1.0 1.0]

                    [-1.0 -1.0 -1.0]
                    [-1.0 1.0 -1.0]
                    [1.0 1.0 -1.0]
                    [1.0 -1.0 -1.0]

                    [-1.0 1.0 -1.0]
                    [-1.0 1.0 1.0]
                    [1.0 1.0 1.0]
                    [1.0 1.0 -1.0]

                    [-1.0 -1.0 -1.0]
                    [-1.0 -1.0 1.0]
                    [1.0 -1.0 1.0]
                    [1.0 -1.0 -1.0]

                    [1.0 -1.0 -1.0]
                    [1.0 1.0 -1.0]
                    [1.0 1.0 1.0]
                    [1.0 -1.0 1.0]

                    [-1.0 -1.0 -1.0]
                    [-1.0 1.0 -1.0]
                    [-1.0 1.0 1.0]
                    [-1.0 -1.0 1.0]]
           :indices [[0 1 2] [0 2 3]
                     [4 5 6] [4 6 7]
                     [8 9 10] [8 10 11]
                     [12 13 14] [12 14 15]
                     [16 17 18] [16 18 19]
                     [20 21 22] [20 22 23]]
           :n 36})
(defn gen-cube
  [center side]
  (let [halfSide (/ side 2)
        min (- center halfSide)
        max (+ center halfSide)]
    {:points [[min min min] ; [0] front bottom left
              [max min min] ; [1] front bottom right
              [max max min] ; [2] front top right
              [min max min] ; [3] front top left
              [min min max] ; [4] back bottom left
              [max min max] ; [5] back bottom right
              [max max max] ; [6] back top right
              [min max max]]; [7] back top left
     :indices [[0 1 2] [0 2 3] ;front face
               [0 4 7] [3 4 7] ;left faceI
               [1 5 6] [1 2 6] ; right face
               [2 3 6] [2 6 7]
               [0 1 4] [0 4 5]
               [4 5 6] [4 6 7]]; back face
     :n 36
     }))
