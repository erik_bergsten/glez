(ns glez.wires)

;; SHADER AND DRAW FUNCTION FOR ONLY DRAWING LINES BETWEEN ALL THE 
;; VERTICES IN THE MODEL

(defn draw
  [gl {:keys [aVertex]} uniforms
   {:keys [n vertex-buffer index-buffer]}]
  (do
    (. gl bindBuffer (.-ARRAY_BUFFER gl) vertex-buffer)
    (. gl vertexAttribPointer aVertex 3 (.-FLOAT gl) false 0 0)
    (. gl enableVertexAttribArray aVertex)

    (. gl bindBuffer (.-ELEMENT_ARRAY_BUFFER gl) index-buffer)

    (. gl drawElements (.-LINE_STRIP gl) n (.-UNSIGNED_SHORT gl) 0)))


(def shader {
:vertex
'((attribute vec4 :aVertex) ; position of the vertex
  (uniform mat4 :uWorldView) ;projection matrix (for the camera)
  (uniform mat4 :uModelView)  ;total translation and rotation fro the object
  (function void main ()
    (assign :gl_Position (* :uWorldView  ;multiply the vertex by all
                            :uModelView   ;needed transformaitons. maybe
                            :aVertex))))  ;pre multiply in cpu?? DO IT
:fragment
'((precision mediump float)
  (function void main ()
    (assign gl_FragColor (vec4 1.0 0.0 0.0 1.0))))})
