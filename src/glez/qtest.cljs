(ns glez.qtest
  (:require [glez.shader :refer [clj->shader]]
            [glez.shapes :as s]
            [linjar.mat4 :as mat4]
            [linjar.mat3 :as mat3]
            [glez.utils :as u]))


(def WIDTH 800)
(def HEIGHT 600)
(def ID "output")

(def vertex '(
  (attribute vec4 :aPosition)

  (uniform mat4 :uModelView)
  (uniform mat4 :uProjection)

  (function void main ()
    (assign :gl_Position (* :uProjection :uModelView :aPosition)))))

(def fragment '(
  (function void main ()
    (assign gl_FragColor (vec4 1.0 1.0 1.0 1.0)))))



(defn draw-triangles
  [gl {prog :program
       {aPosition :aPosition} :attributes
       {uProjection :uProjection
        uModelView :uModelView} :uniforms}
      {:keys [perspective modelView]}
      triangle
      cube]
  (do
    (. gl useProgram prog)
    (. gl bindBuffer (.-ARRAY_BUFFER gl) triangle)
    (. gl vertexAttribPointer aPosition 3 (.-FLOAT gl) false 0 0)
    (. gl enableVertexAttribArray aPosition)

    (. gl uniformMatrix4fv uProjection false (js/Float32Array. perspective))
    (let [data (js/Float32Array. modelView)]
      (. gl uniformMatrix4fv uModelView false data))

    (. gl drawArrays (.-TRIANGLES gl) 0 3)
    (s/draw-shape gl (:attributes prog) cube)
    ))

(println "\n")
(println (clj->shader vertex))

(let [[canvas gl] (u/init-canvas ID WIDTH HEIGHT)
      program-info (u/create-program gl vertex fragment)
      cube (s/init-shape gl s/cube)
      vertexBuffer (. gl createBuffer)
      matrices {:perspective (mat4/perspective 45 WIDTH HEIGHT 0.1 100.0)
                :modelView (mat4/translate-3d (mat3/rotation-y (/ Math/PI 4)) [0 0 -5])}]
    (. gl bindBuffer (.-ARRAY_BUFFER gl) vertexBuffer)
    (. gl bufferData (.-ARRAY_BUFFER gl)
       (js/Float32Array. [0.0 0.0 -2.0
                          1.0 0.0 -2.0
                          0.0 1.0 -2.0])
       (.-STATIC_DRAW gl))

    (draw-triangles gl program-info matrices vertexBuffer cube)
  )

