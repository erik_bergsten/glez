(ns glez.scene
  (:require [linjar.mat4 :as mat4]
            [glez.model :as m]))

(def empty-origin {:matrix mat4/id
                   :objects []
                   :children []})

(defn show
  [{:keys [matrix]}]
  (mat4/show matrix))

(defn create
  [matrix objects children]
  {:matrix matrix
   :objects objects
   :children children})

;;; generate-matrices :: list-pairs -> matrix -> scene -> list-of-pairs
(defn generate-matrices
  ([scene]
   (generate-matrices '() mat4/id scene))
  ([matrix scene]
   (generate-matrices '() matrix scene))
  ([matrices parent-matrix {:keys [matrix objects children]}]
   (let [matrix' (mat4/multiply parent-matrix matrix)]
     (conj (reduce #(generate-matrices %1 matrix' %2) matrices children)
           [matrix' objects]))))

