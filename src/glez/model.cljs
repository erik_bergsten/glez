(ns glez.model)



(defn load
  [gl {:keys [n vertices indices normals texture-coords]}]
  (let [vertex-buffer (. gl createBuffer)
        normal-buffer (. gl createBuffer)
        texture-buffer (. gl createBuffer)
        index-buffer (. gl createBuffer)]

    (. gl bindBuffer (.-ARRAY_BUFFER gl) vertex-buffer)
    (. gl bufferData (.-ARRAY_BUFFER gl)
                     (js/Float32Array. vertices)
                     (.-STATIC_DRAW gl))

    (. gl bindBuffer (.-ARRAY_BUFFER gl) normal-buffer)
    (. gl bufferData (.-ARRAY_BUFFER gl)
                     (js/Float32Array. normals)
                     (.-STATIC_DRAW gl))

    (. gl bindBuffer (.-ARRAY_BUFFER gl) texture-buffer)
    (. gl bufferData (.-ARRAY_BUFFER gl)
                     (js/Float32Array. texture-coords)
                     (.-STATIC_DRAW gl))

    (. gl bindBuffer (.-ELEMENT_ARRAY_BUFFER gl) index-buffer)
    (. gl bufferData (.-ELEMENT_ARRAY_BUFFER gl)
                     (js/Uint16Array. indices)
                     (.-STATIC_DRAW gl))
    {:n n
     :vertex-buffer vertex-buffer
     :normal-buffer normal-buffer
     :texture-buffer texture-buffer
     :index-buffer index-buffer}))

(defn gen-box
  [[x y z] width height depth]
  (let [x1 (- x (/ width 2))
        x2 (+ x (/ width 2))
        y1 (- y (/ height 2))
        y2 (+ y (/ height 2))
        z1 (- z (/ depth 2))
        z2 (+ z (/ depth 2))]
    {:n 36
     :vertices [x1 y1 z1 ;; front face
                x2 y1 z1
                x1 y2 z1
                x2 y2 z1

                x1 y1 z2 ;; front face
                x2 y1 z2
                x1 y2 z2
                x2 y2 z2

                x2 y2 z2 ;; top face
                x2 y2 z1
                x1 y2 z2
                x1 y2 z1

                x2 y1 z2 ;; bottm face
                x2 y1 z1
                x1 y1 z2
                x1 y1 z1

                x2 y1 z1 ;; rgiht face
                x2 y2 z1
                x2 y1 z2
                x2 y2 z2

                x1 y1 z1 ;; rgiht face
                x1 y2 z1
                x1 y1 z2
                x1 y2 z2]

    :indices [0  2  1   1  2  3
              4  5  6   5  7  6
              8  9  10  9 11 10
              12 14 13  13 14 15
              16 17 18  17 19 18
              20 22 21  21 22 23]
    :normals [0 0 -1
              0 0 -1
              0 0 -1
              0 0 -1

              0 0 1
              0 0 1
              0 0 1
              0 0 1

              0 1 0
              0 1 0
              0 1 0
              0 1 0

              0 -1 0
              0 -1 0
              0 -1 0
              0 -1 0

              1 0 0
              1 0 0
              1 0 0
              1 0 0

              -1 0 0
              -1 0 0
              -1 0 0
              -1 0 0]
    :texture-coords [0.0 0.0
                     1.0 0.0
                     0.0 1.0
                     1.0 1.0

                     0.0 0.0
                     1.0 0.0
                     0.0 1.0
                     1.0 1.0

                     0.0 0.0
                     1.0 0.0
                     0.0 1.0
                     1.0 1.0

                     0.0 0.0
                     1.0 0.0
                     0.0 1.0
                     1.0 1.0

                     0.0 0.0
                     1.0 0.0
                     0.0 1.0
                     1.0 1.0

                     0.0 0.0
                     1.0 0.0
                     0.0 1.0
                     1.0 1.0]}))


(defn gen-sphere
  [radius sector-count stack-count]
  (let [length-inverse (/ 1.0 radius)
        sector-step (* (/ Math/PI sector-count) 2)
        stack-step (/ Math/PI stack-count)
        points (apply concat
                  (for [i (range 0 (inc stack-count))]
                    (let [stack-angle (- (/ Math/PI 2) (* i stack-step))
                          xy (* radius (Math/cos stack-angle))
                          z (* radius (Math/sin stack-angle))]
                      (for [j (range 0 (inc sector-count))]
                        (let [sector-angle (* j sector-step)
                              x (* xy (Math/cos sector-angle))
                              y (* xy (Math/sin sector-angle))]
                          [[x y z] ;; vertex
                           [(* x length-inverse)
                            (* y length-inverse)
                            (* z length-inverse)] ;; normal
                           [(/ j sector-count)
                            (/ i sector-count)] ;; tex-coord
                           ])))))]
    {:n (* sector-count (- stack-count 1) 6)
     :vertices (flatten (map first points))
     :normals (flatten (map second points))
     :texture-coords (flatten (map #(nth % 2) points))
     :indices (remove nil? (flatten (for [i (range 0 stack-count)]
               (let [k1 (* i (inc sector-count))
                     k2 (+ k1 sector-count 1)]
                 (for [j (range 0 sector-count)]
                   (let [kp1 (+ k1 j)
                         kp2 (+ k2 j)]
                     [(if (not= i 0)
                       [kp1 kp2 (inc kp1)]
                       nil)
                      (if (not= i (dec stack-count))
                        [(inc kp1) kp2 (inc kp2)]
                        nil)]))))))}))

(defn gen-plane-repeating
  [segments-x segments-z segment-size]
  (let [vertices (for [x (range 0 segments-x)
                       z (range 0 segments-z)]
                   (let [x1 x
                         z1 z
                         x2 (+ x segment-size)
                         z2 (+ z segment-size)]
                     [x1 0 z1
                      x1 0 z2
                      x2 0 z1
                      x2 0 z2]))
        texture-coords (take (count vertices) (repeat [0 0
                                                       0 1
                                                       1 0
                                                       1 1]))
        normals (take (count vertices) (repeat [0 1 0
                                                0 1 0
                                                0 1 0
                                                0 1 0]))
        indices (flatten
                  (for [i (range 0 (* segments-x segments-z))]
                    (let [i0 (* i 4)]
                      [i0       (+ i0 1) (+ i0 2)
                       (+ i0 2) (+ i0 1) (+ i0 3)])))]
    {:n (count indices)
     :vertices (flatten vertices)
     :texture-coords (flatten texture-coords)
     :normals (flatten normals)
     :indices indices}))

