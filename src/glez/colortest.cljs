(ns glez.colortest
  (:require [glez.unicolor :as uc]
            [glez.model :as m]
            [glez.scene :as sc]
            [glez.loop :as l]
            [linjar.mat4 :as mat4]
            [linjar.mat3 :as mat3]
            [linjar.vec3 :as vec3]
            [linjar.vec2 :as vec2]
            [linjar.camera :as cam]
            [glez.utils :as u]))

(defonce camera (atom (cam/third-person [0 0 0] 0 45 10)))
(def camera-phase (vec2/from-degrees 1))
(def camera-phase-inv (vec2/invert-angle camera-phase))
(def camera-step 0.1)

(defn camera-input
  [input]
  (let [{:keys [yaw pitch]} @camera]
    (if (:arrowLeft input)
      (swap! camera #(cam/yaw % camera-phase))
      (if (:arrowRight input)
        (swap! camera #(cam/yaw % camera-phase-inv))))
    (if (:arrowDown input)
      (swap! camera #(cam/pitch % camera-phase))
      (if (:arrowUp input)
        (swap! camera #(cam/pitch % camera-phase-inv))))
    (if (:keyW input)
      (swap! camera #(cam/move % (- camera-step)))
      (if (:keyS input)
        (swap! camera #(cam/move % camera-step))))
    (if (:keyD input)
      (swap! camera #(cam/strafe % camera-step))
      (if (:keyA input)
        (swap! camera #(cam/strafe % (- camera-step)))))))



(def state (atom {:gl nil
                  :attributes {}
                  :uniforms {}
                  :shapes []
                  :projection mat4/id
                  :model-view mat4/id}))

(defonce running (atom false))
(defn stop
  []
  (reset! running false))

(defn render
  [input evts]
  (let [{:keys [gl shapes projection model-view attributes]
         {:keys [uWorldView uModelView] :as uniforms} :uniforms} @state
        camera-matrix (cam/third-person-matrix @camera)]

      (. gl uniformMatrix4fv uWorldView
                             false
                             (js/Float32Array. (mat4/multiply projection
                                                              camera-matrix)))
      (. gl uniformMatrix4fv uModelView
                             false
                             (js/Float32Array. model-view))
      (. gl clearColor 0.25 0.5 0.55 1.0)
      (. gl clear (bit-or (.-COLOR_BUFFER_BIT gl) (.-DEPTH_BUFFER_BIT gl)))
      (doseq [shape shapes]
        (uc/draw gl attributes uniforms shape))

      (camera-input input)
      (when (:keyEscape input)
        (stop))
      @running))

(defn init
  [id width height]
    (let [[canvas gl] (u/init-canvas id width height)
          {program :program
           attributes :attributes
           {:keys [uWorldView uModelView
                   uAmbientLight uReverseLightDirection] :as uniforms} :uniforms}
              (u/create-program gl uc/shader)
          projection (mat4/perspective 45 width height 0.1 100)
          model-view (mat4/from-3d (mat3/rotation-y (/ Math/PI 8)))
          cube (uc/load gl (m/gen-box [5 0 0] 2 2 2) [1.0 0.0 0.0 1.0])
          floor (uc/load gl (m/gen-plane-repeating 10 10 1) [0.5 0.5 0.5 1.0])
          sphere (uc/load gl (m/gen-sphere 1 36 18) [1.0 1.0 0.0 1.0])]
      (. gl useProgram program)
      (. gl enable (.-CULL_FACE gl))
      (. gl cullFace (.-BACK gl))
      (. gl uniform3f uReverseLightDirection 0.3 0.5 1.0)
      (. gl uniform1f uAmbientLight 0.1)
      (reset! state {:gl gl
                     :shapes [floor sphere cube]
                     :model-view model-view
                     :projection projection
                     :attributes attributes
                     :uniforms uniforms})
      (when-not @running
        (. js/console log "Restarting loop...")
        (reset! running true)
        (l/run render))))

(init "output" 640 360)
