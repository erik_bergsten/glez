(ns glez.utils
  (:require [glez.shader :refer [clj->shader]]))

(defn get-shader-type
  [gl type]
  (case type
    :vertex (.-VERTEX_SHADER gl)
    :fragment (.-FRAGMENT_SHADER gl)))

(defn create-shader
  [gl type code]
  (let [shader-type (get-shader-type gl type)
        shader (.createShader gl shader-type)
        src (clj->shader code)]
    (. gl shaderSource shader src)
    (. gl compileShader shader)
    (if (. gl getShaderParameter shader (.-COMPILE_STATUS gl))
      shader
      (let [err (. gl getShaderInfoLog shader)]
        (. gl deleteShader shader)
        (throw (js/Error err))))))

(def settable #{'attribute 'uniform})
(defn extract-var
  [statement]
  (loop [[head & tail] statement]
    (if (empty? tail)
      head
      (recur tail))))

(defn get-variable
  [[head & tail]]
  (case head
    assign nil
    function nil
    (if (contains? settable head)
      (let [variable (extract-var tail)]
        (if (keyword? variable)
          [head (extract-var tail)]
          nil))
      nil)))

(defn get-variables
  [shader-code]
  (remove nil? (map get-variable shader-code)))

(defn set-variable
  [gl program variables [type key]]
  (case type
    attribute (assoc-in variables [:attributes key]
                        (. gl getAttribLocation program (name key)))
    uniform (assoc-in variables [:uniforms key]
                      (. gl getUniformLocation program (name key)))))
(defn set-variables
  [gl program variables]
  (reduce (partial set-variable gl program) {} variables))

(defn create-program
  [gl {:keys [vertex fragment]}]
  (try
    (let [program (.createProgram gl)
          vertex-shader (create-shader gl :vertex vertex)
          fragment-shader (create-shader gl :fragment fragment)
          vertex-variables (get-variables vertex)
          fragment-variables (get-variables fragment)]
      (. gl attachShader program vertex-shader)
      (. gl attachShader program fragment-shader)
      (. gl linkProgram program)
      (if (. gl getProgramParameter program (.-LINK_STATUS gl))
        program
        (let [err (. gl getProgramInfoLog program)]
          (. gl deleteProgram program)
          (throw (js/Error err))))
      (into {:program program}
            (set-variables gl
                           program
                           (concat vertex-variables
                                   fragment-variables))))
    (catch js/Error e (. js/console log "Failde to create webgl program:" e))))

(defn init-gl
  [gl width height]
  (do
    (. gl viewport 0 0 width height)
    (. gl enable (.-BLEND gl))
    (. gl blendFunc (.-SRC_ALPHA gl) (.-ONE_MINUS_SRC_ALPHA gl))
    (. gl clearColor 0.25 0.5 0.55 1.0)
    (. gl clear (bit-or (.-COLOR_BUFFER_BIT gl) (.-DEPTH_BUFFER_BIT gl)))
    (. gl enable (.-DEPTH_TEST gl))
    (. gl depthFunc (.-LEQUAL gl))
    ))

(defn init-canvas
  [id width height]
  (let [canvas (. js/document getElementById id)
        gl (. canvas getContext "webgl")]
    (set! (.-width canvas) width)
    (set! (.-height canvas) height)
    (init-gl gl width height)
    [canvas gl]))

(defn power-of-2?
  [value]
  (= 0 (bit-and value (dec value))))

(defn load-texture
  [gl src]
  (let [texture (. gl createTexture)
        image (js/Image.)]
    (. gl bindTexture (.-TEXTURE_2D gl) texture)
    (. gl texImage2D (.-TEXTURE_2D gl) 0 (.-RGBA gl)
                     1 1 0 (.-RGBA gl) (.-UNSIGNED_BYTE gl)
                     (js/Uint8Array. [0 0 255 255]))
    (set! (.-onload image)
          #(do ;(println "image loaddued!")
               (. gl bindTexture (.-TEXTURE_2D gl) texture)
               (. gl texImage2D (.-TEXTURE_2D gl) 0 (.-RGBA gl)
                                (.-RGBA gl) (.-UNSIGNED_BYTE gl)
                                image)
               ;(. gl texParameteri (.-TEXTURE_2D gl)
               ;                    (.-TEXTURE_MAG_FILTER gl)
               ;                    (.-NEAREST gl))
               (if (and (power-of-2? (.-width image))
                        (power-of-2? (.-height image)))
                 (. gl generateMipmap (.-TEXTURE_2D gl))
                 (do (. gl texParameteri (.-TEXTURE_2D gl)
                                         (.-TEXTURE_WRAP_S gl)
                                         (.-CLAMP_TO_EDGE gl))
                     (. gl texParameteri (.-TEXTURE_2D gl)
                                         (.-TEXTURE_WRAP_T gl)
                                         (.-CLAMP_TO_EDGE gl))
                     (. gl texParameteri (.-TEXTURE_2D gl)
                                         (.-TEXTURE_MIN_FILTER gl)
                                         (.-LINEAR gl))))))
    (set! (.-src image) src)
    texture))


