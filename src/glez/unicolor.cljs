(ns glez.unicolor
  (:require [glez.model :as m]))

(defn load
  [gl model color]
  (into {:color (js/Float32Array. color)} (m/load gl model)))

(defn draw
  [gl {:keys [aVertex aNormal]} {uColor :uColor}
   {:keys [n vertex-buffer index-buffer normal-buffer color]}]
  (do
    (. gl bindBuffer (.-ARRAY_BUFFER gl) vertex-buffer)
    (. gl vertexAttribPointer aVertex 3 (.-FLOAT gl) false 0 0)
    (. gl enableVertexAttribArray aVertex)

    (. gl bindBuffer (.-ARRAY_BUFFER gl) normal-buffer)
    (. gl vertexAttribPointer aNormal 3 (.-FLOAT gl) false 0 0)
    (. gl enableVertexAttribArray aNormal)

    (. gl bindBuffer (.-ELEMENT_ARRAY_BUFFER gl) index-buffer)

    (. gl uniform4fv uColor color)

    (. gl drawElements (.-TRIANGLES gl) n (.-UNSIGNED_SHORT gl) 0)))

(def shader {
:vertex
'((attribute vec4 :aVertex) ; position of the vertex
  (attribute vec3 :aNormal) ; normal (for lighting)
  (uniform mat4 :uWorldView) ;projection matrix (for the camera)
  (uniform mat4 :uModelView)  ;total translation and rotation fro the object
  (varying vec3 :vNormal)  ;forwarding normal to fragment shader for interpolation
  (function void main ()
    (assign :vNormal (* (mat3 :uModelView) :aNormal)) ;assign the attribute to the varying
    (assign :gl_Position (* :uWorldView  ;multiply the vertex by all
                            :uModelView   ;needed transformaitons. maybe
                            :aVertex))))  ;pre multiply in cpu?? DO IT
:fragment
'((precision mediump float)

  (uniform vec3 :uReverseLightDirection)
  (uniform float :uAmbientLight)
  (uniform vec4 :uColor)

  (varying vec3 :vNormal)
  (function void main ()
    (assign (float light) (max f.0 (dot :vNormal :uReverseLightDirection)))
    (assign light (min f.1 (+ light :uAmbientLight)))
    (assign gl_FragColor.rgb (* uColor.rgb light))
    (assign gl_FragColor.a uColor.a)))})

