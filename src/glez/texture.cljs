(ns glez.texture
  (:require [glez.model :as m]))

(defn load
  [gl model texture]
  (into {:texture texture} (m/load gl model)))

(defn draw
  [gl {:keys [aVertex aNormal aTextureCoord]} {uTexture :uTexture}
   {:keys [n vertex-buffer index-buffer normal-buffer texture-buffer texture]}]
  (do
    (. gl bindBuffer (.-ARRAY_BUFFER gl) vertex-buffer)
    (. gl vertexAttribPointer aVertex 3 (.-FLOAT gl) false 0 0)
    (. gl enableVertexAttribArray aVertex)

    (. gl bindBuffer (.-ARRAY_BUFFER gl) normal-buffer)
    (. gl vertexAttribPointer aNormal 3 (.-FLOAT gl) false 0 0)
    (. gl enableVertexAttribArray aNormal)

    (. gl bindBuffer (.-ARRAY_BUFFER gl) texture-buffer)
    (. gl vertexAttribPointer aTextureCoord 2 (.-FLOAT gl) false 0 0)
    (. gl enableVertexAttribArray aTextureCoord)

    (. gl bindBuffer (.-ELEMENT_ARRAY_BUFFER gl) index-buffer)

    (. gl activeTexture (.-TEXTURE0 gl))
    (. gl bindTexture (.-TEXTURE_2D gl) texture)
    (. gl uniform1i uTexture 0)

    (. gl drawElements (.-TRIANGLES gl) n (.-UNSIGNED_SHORT gl) 0)))

(def shader {
:vertex
'((attribute vec4 :aVertex) ; position of the vertex
  (attribute vec2 :aTextureCoord) ;uuu
  (attribute vec3 :aNormal) ; normal (for lighting)
  (uniform mat4 :uWorldView) ;projection matrix (for the camera)
  (uniform mat4 :uModelView)  ;total translation and rotation fro the object
  (varying vec3 :vNormal)  ;forwarding normal to fragment shader for interpolation
  (varying vec2 :vTextureCoord)
  (function void main ()
    (assign :vNormal (* (mat3 :uModelView) :aNormal)) ;assign the attribute to the varying
    (assign :vTextureCoord :aTextureCoord)
    (assign :gl_Position (* :uWorldView  ;multiply the vertex by all
                            :uModelView   ;needed transformaitons. maybe
                            :aVertex))))  ;pre multiply in cpu?? DO IT
:fragment
'((precision mediump float)

  (uniform vec3 :uReverseLightDirection)
  (uniform float :uAmbientLight)
  (uniform sampler2D :uTexture)

  (varying vec3 :vNormal)
  (varying vec2 :vTextureCoord)

  (function void main ()
    (assign (float light) (max f.0 (dot :vNormal :uReverseLightDirection)))
    (assign light (min f.1 (+ light :uAmbientLight)))
    (assign (vec4 color) (texture2D :uTexture :vTextureCoord))
    (assign gl_FragColor.rgb (* color.rgb light))
    (assign gl_FragColor.a color.a)))})

