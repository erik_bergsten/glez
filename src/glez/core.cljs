(ns glez.core
  (:require [glez.shader :as sh]
            [glez.model :as m]
            [glez.scene :as sc]
            [glez.texture :as t]
            [glez.loop :as l]
            [linjar.mat4 :as mat4]
            [linjar.mat3 :as mat3]
            [linjar.vec3 :as vec3]
            [linjar.vec2 :as vec2]
            [linjar.camera :as cam]
            [glez.utils :as u]))

(def camera (atom (assoc (cam/third-person [0 0 0] 0 90 10) :zoom 5.0)))

(def camera-rotation (vec2/from-degrees 1))
(def camera-rotation-inv (vec2/from-degrees -1))
(def camera-move-step 0.1)


(defn update-camera
  [input]
  (let [{:keys [yaw pitch]} @camera]
    (if (:arrowLeft input)
      (swap! camera #(cam/yaw % camera-rotation))
      (if (:arrowRight input)
        (swap! camera #(cam/yaw % camera-rotation-inv))))
    (if (:arrowDown input)
      (swap! camera #(cam/pitch % camera-rotation))
      (if (:arrowUp input)
        (swap! camera #(cam/pitch % camera-rotation-inv))))
    (if (:keyW input)
      (swap! camera #(cam/move % (- camera-move-step)))
      (if (:keyS input)
        (swap! camera #(cam/move % camera-move-step))))
    (if (:keyD input)
      (swap! camera #(cam/strafe % camera-move-step))
      (if (:keyA input)
        (swap! camera #(cam/strafe % (- camera-move-step)))))))

(def state (atom {}))

(defn render
  [input _]
  (let [{:keys [gl projection scene sphere attributes]
         {:keys [uWorldView uModelView uAmbientLight] :as uniforms} :uniforms} @state
        {pos :position} @camera
        camera (cam/third-person-matrix @camera)]
      (. gl clearColor 0.25 0.5 0.55 1.0)
      (. gl clear (bit-or (.-COLOR_BUFFER_BIT gl) (.-DEPTH_BUFFER_BIT gl)))
      (. gl uniformMatrix4fv uWorldView
                             false
                             (js/Float32Array. (mat4/multiply projection
                                                              camera)))

      (. gl uniformMatrix4fv uModelView false
         (js/Float32Array. (mat4/translate (mat4/from-3d
                                             (mat3/rotation-z (/ Math/PI 4)))  pos)))

      (. gl uniform1f uAmbientLight 0.1)
      (t/draw gl attributes uniforms sphere)
      (. gl uniform1f uAmbientLight 0.5)
      (doseq [[mat objects] (sc/generate-matrices scene)]
        (. gl uniformMatrix4fv uModelView false (js/Float32Array. mat))
        (doseq [obj objects]
          (t/draw gl attributes uniforms obj)))

      ;(sc/draw gl attributes uniforms mat4/id scene)
      (update-camera input)
      (not (:keyEscape input))))

(defn init
  [id width height]
    (let [[canvas gl] (u/init-canvas id width height)
          {program :program
           attributes :attributes
           {:keys [uWorldView uModelView
                   uReverseLightDirection
                   uTexture uAmbientLight] :as uniforms} :uniforms}
              (u/create-program gl t/shader)
          reverse-light (vec3/normalize [0.3 0.3 1.0])
          projection (mat4/perspective 45 800 600 0.1 100)
          phase (vec2/from-angle (/ Math/PI 4))
          modelView (mat4/roll mat4/id phase)
          texture (u/load-texture gl "public/res/pixel.png")
          cube (t/load gl (m/gen-box [0 0 0] 0.5 0.5 0.5) texture)
          plane (t/load gl (m/gen-plane-repeating 1 1 10) texture)
          sphere (t/load gl (m/gen-sphere 1 36 18) texture)
          scene (sc/create mat4/id
                           [plane cube]
                           [])]
      (println (sc/generate-matrices scene))
      (. gl useProgram program)
      (. gl enable (.-CULL_FACE gl))
      (. gl cullFace (.-BACK gl))
      (. gl uniform3fv uReverseLightDirection (js/Float32Array. reverse-light))
      ;(. gl uniform1f uAmbientLight 0.1)
      (. gl clearColor 0.25 0.5 0.55 1.0)
      (. gl clear (bit-or (.-COLOR_BUFFER_BIT gl) (.-DEPTH_BUFFER_BIT gl)))
      (reset! state {:attributes attributes
                     :uniforms uniforms
                     :scene scene
                     :gl gl
                     :sphere sphere
                     :projection projection})
      (l/run render)))

(init "output" 800 600)
