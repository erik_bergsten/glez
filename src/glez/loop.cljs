(ns glez.loop)


(def code->key {"KeyW"       :keyW
                "KeyA"       :keyA
                "KeyS"       :keyS
                "KeyD"       :keyD
                "Escape"     :keyEscape
                "Space"      :keySpace
                "ArrowUp"    :arrowUp
                "ArrowDown"  :arrowDown
                "ArrowLeft"  :arrowLeft
                "ArrowRight" :arrowRight})
(def button->key {0 :buttonLeft
                  1 :buttonMiddle
                  2 :buttonRight})

(defn keydown-handler
  [input events e]
  (let [key (get code->key (.-code e) :unknown)]
    (swap! input assoc key true)
    (swap! events conj [:keydown key])))

(defn keyup-handler
  [input events e]
  (let [key (get code->key (.-code e) :unknown)]
    (swap! input assoc key false)
    (swap! events conj [:keyup key])))

(defn mousedown-handler
  [input events e]
  (let [button (get button->key (.-button e) :unknown)
        x (.-clientX e)
        y (.-clientY e)]
    (swap! input assoc button true)
    (swap! events conj [:mousedown button x y])))
(defn mouseup-handler
  [input events e]
  (let [button (get button->key (.-button e) :unknown)
        x (.-clientX e)
        y (.-clientY e)]
    (swap! input assoc button false)
    (swap! events conj [:mouseup button x y])))

(defn run
  [update]
  (let [input (atom {})
        events (atom cljs.core/PersistentQueue.EMPTY)
        keydown-listener (partial keydown-handler input events)
        keyup-listener (partial keyup-handler input events)
        mousedown-listener (partial mousedown-handler input events)
        mouseup-listener (partial mouseup-handler input events)
        cleanup (fn [] (do
                  (. js/window removeEventListener "mousedown" mousedown-listener)
                  (. js/window removeEventListener "mouseup" mouseup-listener)
                  (. js/window removeEventListener "keydown" keydown-listener)
                  (. js/window removeEventListener "keyup" keyup-listener)))
        ]
    (. js/window addEventListener "mousedown" mousedown-listener)
    (. js/window addEventListener "mouseup" mouseup-listener)
    (. js/window addEventListener "keydown" keydown-listener)
    (. js/window addEventListener "keyup" keyup-listener)
    (. js/window requestAnimationFrame
      (fn tick [n]
        (do
          (if (update @input events)
            (. js/window requestAnimationFrame tick)
            (cleanup)))))))


(def n (atom 0))
(defn test-update
  [input _]
  (do (. js/console log "heh:" @n)
      (if (:keyEscape input)
        nil
        (swap! n inc))))

